<?php

/**
 * � problem�tico colocar este arquivo num diret�rio pai porque o PHP relativiza os paths de acordo com o contexto ao
 * fazer "require" ou "include". Por exemplo, se executar o arquivo src/tests/tests.php a partir do diret�rioa src e usar 
 * require ../../psr-0-autoload.php no arquivo src/tests/tests.php, gerar� um erro, no entanto se executar a partir do 
 * diret�rio src/tests, funcionar� normalmente.
 */

spl_autoload_register(function ($className) {
    include str_replace('\\', DIRECTORY_SEPARATOR, $className)  . '.php';
});

$rootPath = realpath(implode(DIRECTORY_SEPARATOR, [__DIR__, '..', '..']));

$settingsPathOfTests =  implode(DIRECTORY_SEPARATOR, [$rootPath, 'src', 'tests', 'settings']);

$pathInclude = implode(DIRECTORY_SEPARATOR, [$settingsPathOfTests, 'pathInclude.php']);

require $pathInclude;
