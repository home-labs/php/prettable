<?php

namespace QueryStatements\Decorators\Select\Pagination;

use
    PretTable\Helpers\Pagination,
    PretTable\QueryStatements\AbstractComponent,
    PretTable\QueryStatements\AbstractDecorator
;

class MySQL extends AbstractDecorator {

    function __construct(AbstractComponent $component, $limit, $pageNumber = 1) {
        parent::__construct($component, $limit, $pageNumber);

        $this->_statement = $this->resolveStatement($limit, $pageNumber);
    }

    private function resolveStatement() {
        $offset = Pagination::calculateOffset($this->limit, $this->pageNumber);

        if (isset($this->limit)) {
            return "LIMIT $this->limit\n\n\tOFFSET $offset";
        }

        return '';
    }

}
