<?php

namespace Models;

// to mixin code (to don't repeat the same code)
trait ModelTrait {

    function __construct($databaseSchemaName) {
        $data = include 'settings/database.php';

        putenv('_ENV=development');
        $environment = getenv('_ENV');
        parent::__construct($data, $environment);

        $this->establishConnection($databaseSchemaName);
    }

}
