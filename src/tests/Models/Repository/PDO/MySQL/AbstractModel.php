<?php


namespace Models\Repository\PDO\MySQL;


use
    PretTable\Repository\Writable\QuestionMarkPlaceholder\PDO,
    Models\ModelTrait,
    Models\Paginables\PDO\MySQLTrait
;


abstract class AbstractModel extends PDO\AbstractModel {

    use ModelTrait;

    use MySQLTrait;

}
