<?php

// o que não é Repository, pressupõe DAO 
namespace Models\PDO;


use
    PretTable\PDO\AbstractModelBase,
    Models\ModelTrait,
    Models\PDO\Paginables\MySQLTrait
;


abstract class AbstractModel extends AbstractModelBase {

    use ModelTrait;
    
    use MySQLTrait;

}
