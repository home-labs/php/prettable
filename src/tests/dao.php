<?php

require 'psr-0-autoload.php';


use
    Models\PDO\AbstractModel
;


class ExampleModel3DAO extends AbstractModel {

// pegar exemplo de como se usa no projeto clonado da época da SESEG

    function __construct() {
//         $this->
    }

    static function getTableName() {
        return 'table3';
    }
    
    static function getPrimaryKeyName() {
        return 'id';
    }
    
    function getColumnNames() {
        return [
            'id',
            'table3col'
        ];
    }
    
}



$exampleModel3DAO = new ExampleModel3DAO();

for ($i = 1; $i <= 1; $i++) {
//     TODO: o método ainda não existe neste contexto. Criar o método, mas antes começar a implementar o padrão Composite
    $exampleModel3DAO = $exampleModel3DAO->create(
        [
            'table1col' => "a value $i"
        ]
    );
}
echo $exampleModel3DAO->save();

// for ($i = 1; $i <= 10; $i++) {
//     $mode2 = $mode2->create(['table2col' => "a value $i"]);
// }
// echo $mode2->save();

// for ($i = 1; $i <= 2; $i++) {
//     $model3 = $model3->create(
//         [
//             'table3col' => "a value $i",
//             'table1_id' => $i
//         ]
//     );
// }
// echo $model3->save();

// for ($i = 1; $i <= 2; $i++) {
//     $model4 = $model4->create(
//         [
//             'table4col' => "a value $i"
//             , 'table1_id' => 1
//         ]
//     );
// }
// echo $model4->save();


// $model1->setPrimaryKeyValue(1);


// SELECTs

// $model1->setOrderBy('table1.id');

// $model1->join('Model2');
// print_r($model1->read('id', 2));
// print_r($model1->read());

// print_r($model1->readAll(2, 2));
// print_r($model1->readAll());

// print_r($model1->count());
