<?php

$setIncludePath = function (array $paths) {
    set_include_path(get_include_path() . PATH_SEPARATOR . implode(DIRECTORY_SEPARATOR, $paths));
};

$rootPath = realpath(implode(DIRECTORY_SEPARATOR, [__DIR__, '..', '..', '..']));

$setIncludePath([$rootPath]);

$setIncludePath([$rootPath, 'src', 'lib']);

$setIncludePath([$rootPath, 'src', 'tests']);
