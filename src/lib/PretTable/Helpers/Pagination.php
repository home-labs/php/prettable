<?php

namespace PretTable\Helpers;


// TODO: colocar numa nova lib
class Pagination {

    private $collection;

    private $limit;

    private $count;

    private $totalPages;

    private $currentPageNumber;

    // a array with items of page
    private $page;

    function __construct(
        array $collection,
        $limit = null
    ) {
        $this->collection = $collection;
        $this->count = count($this->collection);
        $this->limit = self::resolveLimit($this->count, $limit);
        $this->totalPages = self::calculateTotalPages($this->count, $limit);
        $this->page = [];
        $this->currentPageNumber = 1;
    }

    static function calculateTotalPages($count, $limit = null) {
        $limit = self::resolveLimit($count, $limit);

        if ($limit > 0 && $count > 0) {
            return ceil($count / $limit);
        }

        return 1;
    }

    private static function resolvePageNumber($pageNumber = 1, $totalPages = 1) {

        if ($totalPages < 1) {
            $totalPages = 1;
        }

        if ($pageNumber < 1) {
            $pageNumber = 1;
        } else if ($pageNumber > $totalPages) {
            $pageNumber = $totalPages;
        }

        return $pageNumber;
    }

    static function calculateOffset($count, $limit, $pageNumber) {

        if (isset($count)) {
            $limit = self::resolveLimit($count, $limit);
            $totalPages = self::calculateTotalPages($count, $limit);
            // cause' of these filters isn't possible the offset exceed maximum
        }

        $pageNumber = self::resolvePageNumber($pageNumber, $totalPages);

        $offset = $limit * ($pageNumber - 1);

        if ($offset <= 0) {
            $offset = 0;
        }

        return $offset;
    }

    private static function resolveLimit($count, $limit = 0) {

        if ($count === 0) {
            return $count;
        } else if ($limit <= 0) {
            return 0;
        }

        return $limit;
    }

    function setLimit($value) {
        $this->limit = $value;
    }

    function getPage($pageNumber) {

        if (isset($this->limit)) {
            if (
                (count($this->collection) && !count($this->page))
                || ($pageNumber !== $this->currentPageNumber)
                )
            {

                $this->currentPageNumber = self::resolvePageNumber($pageNumber, $this->totalPages);
                $offset = self
                    ::calculateOffset($this->limit, $this->currentPageNumber, $this->count);
                $this->page = array_slice($this->collection, $offset, $this->limit);
            }
        } else {
            $this->page = $this->collection;
        }

        return $this->page;
    }

}
