<?php

namespace PretTable\Helpers\SQL\QuoteTreatment;

class Single {

    static function treat(array $values) {
        $adjusted = [];

        foreach ($values as $value) {

            switch (gettype($value)) {
                case 'string': {
                    if (mb_detect_encoding($value) === 'UTF-8') {
                        $value = utf8_decode($value);
                    }

                    $value = "'$value'";
                }
                break;
                case 'NULL': {
                    $value = 'NULL';
                }
            }

            array_push($adjusted, $value);
        }

        return $adjusted;
    }

}
