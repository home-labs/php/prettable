<?php


namespace PretTable\Repository\Readonly\QuestionMarkPlaceholder\PDO;


use 
    PretTable\Repository\Readonly\QuestionMarkPlaceholder,
    PretTable\PDO\ModelTraitProxy
;


abstract class AbstractModelBase extends QuestionMarkPlaceholder\AbstractModel {

    use ModelTraitProxy;

}
