<?php


namespace PretTable\Repository\Readonly\QuestionMarkPlaceholder;


use
    PretTable\QueryStatements\Component,
    PretTable\QueryStatements\Decorators\ColumnSelect,
    PretTable\Repository\Readonly
;


abstract class AbstractModel extends Readonly\AbstractModel {

    function read($columnName = null, $value = null) {
        if (!isset($columnName) || !isset($value)) {
            $columnName = $this->getPrimaryKeyName();
            $value = $this->primaryKeyValue;
        }

        $tableName = $this->getTableName();
        $attachTableName = false;
        $whereStatement = "WHERE $columnName = ?";

        if (isset($this->joinsDecorator)) {
            $joinsStatement = "\t{$this->joinsDecorator->getStatement()}";
            $attachTableName = true;
            $whereStatement = "WHERE $tableName.$columnName = ?";
        } else {
            $joinsStatement = '';
        }

        if (!isset($this->columnSelectDecorator)) {
            $component = new Component("SELECT");
        } else {
            $component = $this->columnSelectDecorator;
        }

        $this->columnSelectDecorator = new ColumnSelect($component, $this, $attachTableName);

        $sql = "\n\t{$this->columnSelectDecorator->getStatement()}\n\tFROM $tableName";

        $sql .= $joinsStatement;

        $sql .= "\n\n\t$whereStatement";

        $orderByStatement = $this->getOrderByStatement();

        if (isset($orderByStatement)) {
            $sql .= $orderByStatement;
        }

        $this->setBindings([$value]);

        $result = $this->execute($sql);

        $allFetched = $result->fetchAll();

        if (gettype($allFetched) === 'array'
            && count($allFetched)
        ) {
            return $allFetched[0];
        }

        return null;
    }

    function readFromComponent($modelName) {
        $sql = $this->resolvedRelationalSelect($modelName)->getStatement();

        $sql .= "\n\n\tWHERE {$this->getTableName()}.{$this->getPrimaryKeyName()} = ?";

        $orderByStatement = $this->getOrderByStatement();

        if (isset($orderByStatement)) {
            $sql .= "$orderByStatement";
        }

        return new Component($sql);
    }

    function readParent($modelName) {
        $result = $this->readFromComponent($modelName);

        if (isset($result)
            && gettype($result) == 'array'
            && count($result)
            ) {
            return $result[0];
        }

        return null;
    }
    
    function countComponent() {
        if (isset($this->joinsDecorator)) {
            $joinsStatement = "\t{$this->joinsDecorator->getStatement()}";
        } else {
            $joinsStatement = '';
        }
        
        $this->columnSelectDecorator = new Component("SELECT\n\tcount(*)");
        
        $sql = "\n{$this->columnSelectDecorator->getStatement()}\n\tFROM {$this->getTableName()}";
        
        $sql .= $joinsStatement;
        
        return new Component($sql);
    }
    
    function count() {
        $component = $this->countComponent();
        $sql = $component->getStatement();
        
        $result = $this->execute($sql);
        
        return $result->fetchColumn();
    }
    
    function readAllComponent() {
        $attachTableName = false;
        
        if (isset($this->joinsDecorator)) {
            $joinsStatement = "\t{$this->joinsDecorator->getStatement()}";
            $attachTableName = true;
        } else {
            $joinsStatement = '';
        }
        
        if (!isset($this->columnSelectDecorator)) {
            $component = new Component("SELECT");
        } else {
            $component = $this->columnSelectDecorator;
        }
        
        $this->columnSelectDecorator = new ColumnSelect($component, $this, $attachTableName);
        
        $sql = "\n\t{$this->columnSelectDecorator->getStatement()}\n\tFROM {$this->getTableName()}";
        
        $sql .= $joinsStatement;
        
        $orderByStatement = $this->getOrderByStatement();
        
        if (isset($orderByStatement)) {
            $sql .= $orderByStatement;
        }
        
        return new Component($sql);
    }

}
