<?php


namespace PretTable;


use PretTable\ModelInterface;


interface WritableModelInterface extends ModelInterface {

    static function isPrimaryKeySelfIncremental();

}
