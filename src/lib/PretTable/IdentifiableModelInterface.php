<?php

namespace PretTable;

interface IdentifiableModelInterface extends ModelInterface {

    static function getPrimaryKeyName();

}
