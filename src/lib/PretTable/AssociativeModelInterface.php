<?php

namespace PretTable;

interface AssociativeModelInterface extends ModelInterface {

    static function getAssociativeColumnNames();

}
