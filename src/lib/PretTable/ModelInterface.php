<?php

namespace PretTable;

interface ModelInterface {

    static function getTableName();

    function getColumnNames();

}
