<?php

namespace PretTable\QueryStatements\CharacterFugitive;

interface StrategyInterface {

    function getEscaped(array $values);

}
