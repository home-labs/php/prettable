<?php

namespace PretTable\QueryStatements;

interface StrategyInterface {

    function getStatement(array $attributes);
    
}
