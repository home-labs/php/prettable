<?php

namespace PretTable\QueryStatements\Placeholders;

interface StrategyInterface {

    function getStatement(array $attributes);

}
