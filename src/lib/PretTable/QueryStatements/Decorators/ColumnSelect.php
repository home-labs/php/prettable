<?php

namespace PretTable\QueryStatements\Decorators;

use
    PretTable\Reflection,
    PretTable\InheritanceRelationship,
    PretTable\QueryStatements\AbstractComponent,
    PretTable\QueryStatements\Decorators\Select\AbstractDecorator
;

class ColumnSelect extends AbstractDecorator {

    private $model;

    private $attachTableName;

    function __construct(AbstractComponent $component, $model, $attachTableName = false) {
        parent::__construct($component);

        $this->model = $model;

        $this->attachTableName = $attachTableName;


        $this->_statement =  $this->resolveStatement() . "\n\t\t";
    }

    private function resolveStatement() {
        if (gettype($this->model) == 'string') {
            InheritanceRelationship
                ::throwIfClassIsntA($this->model, 'PretTable\ModelInterface');

            $modelDeclaration = Reflection::getDeclarationOf($this->model);
            $this->model = Reflection::getInstanceOf($this->model);
        } else if (gettype($this->model) == 'object') {
            InheritanceRelationship::throwIfClassIsntA(get_class($this->model),
                'PretTable\ModelInterface');
            $modelDeclaration = Reflection
                ::getDeclarationOf(get_class($this->model));
        }

        $columnNames = $this->model->getColumnNames();

        if ($this->attachTableName) {
            $tableName = $modelDeclaration::getTableName();
        }

        $mountedColumns = [];

        foreach($columnNames as $columnName) {
            $mountedColumns[] = $this->attachTableName ? "$tableName.$columnName AS '{$tableName}.{$columnName}'" : $columnName;

//             array_push($mountedColumns, ($this->attachTableName
//                 ? "$tableName.$columnName AS '{$tableName}.{$columnName}'"
//                 : $columnName)
//             );
        }

        $statement = implode("\n\t\t, ", $mountedColumns);

        return $statement;
    }

}
