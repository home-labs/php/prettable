<?php


namespace PretTable\PDO;


use 
    PretTable\AbstractModel
;


abstract class AbstractModelBase extends AbstractModel {

    use ModelTraitProxy;

}
